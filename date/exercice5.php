<!-- Afficher le nombre de jour qui sépare la date du jour avec le 16 mai 2016. -->

<?php

$sec = date("U", mktime()) - date("U", mktime(0, 0, 0, 6, 5, 2016));

$jours = round($sec / (60*60*24));

echo "Il s'est écoulé $jours jours depuis le 16 mai 2016.";

?>