<!-- Afficher la date du jour + 20 jours. -->

<?php

$futurdate = time() + (20 * 24 * 60 * 60);
// 20 jours; 24 heures; 60 minutes; 60 secondes
echo 'Aujourd\'hui :       '. date('d-m-Y') ."<br>";
echo 'Dans 20 jours : '. date('d-m-Y', $futurdate) ."<br>";

?>