<!-- Afficher le timestamp du jour.
Afficher le timestamp du mardi 2 août 2016 à 15h00. -->

<?php

echo date("c", mktime()) . "<br>";

echo date("d-m-Y", mktime(15, 0, 0, 8, 2, 2016)) . " à " . date("G", mktime(15, 0, 0, 8, 2, 2016)) . " heure.";
?>