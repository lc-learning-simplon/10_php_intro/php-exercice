<!-- Afficher le nombre de jour dans le mois de février de l'année 2016. -->

<?php

$date1 = "01-02-2016"; 
$date2 = "01-03-2016";

// On transforme les 2 dates en timestamp
$date3 = strtotime($date1);
$date4 = strtotime($date2);
 
// On récupère la différence de timestamp entre les 2 précédents
$nbJoursTimestamp = $date4 - $date3;
 
// ** Pour convertir le timestamp (exprimé en secondes) en jours **
// On sait que 1 heure = 60 secondes * 60 minutes et que 1 jour = 24 heures donc :
$nbJours = $nbJoursTimestamp/86400; // 86 400 = 60*60*24
 
echo "Nombre de jours : ".$nbJours;
?>