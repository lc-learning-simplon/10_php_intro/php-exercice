<!-- Afficher la date du jour - 22 jours. -->

<?php

$anciennedate = time() - (22 * 24 * 60 * 60);
// 22 jours; 24 heures; 60 minutes; 60 secondes
echo 'Aujourd\'hui :       '. date('d-m-Y') ."<br>";
echo 'Dans 20 jours : '. date('d-m-Y', $anciennedate) ."<br>";

?>