<!-- Faire une page index.php. Tester sur cette page que tous les paramètres de cette URL existent et 
les afficher: index.php?dateDebut=2/05/2016&dateFin=27/11/2016 -->

<?php

if(isset($_GET["dateDebut"]) && isset($_GET["dateFin"])) {
    echo "<p>Début = " . $_GET["dateDebut"] . ".</p>";
    echo "<p>Fin = " . $_GET["dateFin"] . ".</p>";
    }
if(!isset($_GET["dateDebut"]) || !isset($_GET["dateFin"])) {
    echo "<p>Le querystring est incomplet ! Vous devez indiquez une date de début et de fin au format JJ/MM/AAAA</p>";
    }
    
?>