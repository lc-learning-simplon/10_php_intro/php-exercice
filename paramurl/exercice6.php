<!-- Faire une page index.php. Tester sur cette page que tous les paramètres de cette URL existent et 
les afficher: index.php?batiment=12&salle=101 -->

<?php

if (isset($_GET["batiment"]) && isset($_GET["salle"])) {
    echo "<p>Bâtiment " . $_GET["batiment"] . ", salle " . $_GET["salle"] . ".</p>";
}
if (!isset($_GET["batiment"]) && !isset($_GET["salle"])) {
    echo "<p>Le querystring n'est pas complet ! Veuillez indiquer le numéro du bâtiment et de salle.</p>";
}
?>