<!-- Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur :

    - Homme
    - Femme

    La fonction doit renvoyer en fonction des paramètres :

    Vous êtes un homme et vous êtes majeur
    Vous êtes un homme et vous êtes mineur
    Vous êtes une femme et vous êtes majeur
    Vous êtes une femme et vous êtes mineur

 -->

<?php

function sendme ($age, $genre) {
    if ($genre == "Femme" && $age >= 18) {
        echo "<p>Vous êtes une femme et vous êtes majeur.</p>";
    }
    else if ($genre == "Femme" && $age < 18) {
        echo "<p>Vous êtes une femme et vous êtes mineur.</p>";
    }
    else if ($genre == "Homme" && $age >= 18) {
        echo "<p>Vous êtes un homme et vous êtes majeur.</p>";
    }
    else if ($genre == "Homme" && $age < 18) {
        echo "<p>Vous êtes un homme et vous êtes mineur.</p>";
    }
    else {
        echo "<p>Genre non définit !</p>";
    }
}

sendme(20, "Femme");
sendme(10, "Femme");
sendme(20, "Homme");
sendme(10, "Homme");
sendme(20, "h");

?>