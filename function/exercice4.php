<!-- Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :

    - Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
    - Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
    - Les deux nombres sont identiques si les deux nombres sont égaux
 -->

<?php

function sendme ($n, $m) {
    if ($n < $m) {
        echo "<p>Le premier nombre est plus petit.</p>";
    }
    if ($n > $m) {
        echo "<p>Le premier nombre est plus grand.</p>";
    }
    if ($n == $m) {
        echo "<p>Les deux nombres sont identiques.</p>";
    }
}

sendme(2, 7);
sendme(7, 2);
sendme(3, 3);
