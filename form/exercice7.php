<!-- Au formulaire de l'exercice 5, ajouter un champ d'envoi de fichier. 
Afficher en plus de ce qui est demandé à l'exercice 6, le nom et l'extension du fichier. -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <?php

    if (isset($_GET["civ"]) || isset($_GET["nom"]) || isset($_GET["prenom"])) {
        echo "<p>Bonjour " . $_GET["civ"] . " " . $_GET["prenom"] . " " . $_GET["nom"]. ". Ci-joint le fichier " . $_GET["file"] . " !</p>";
    }
    if (!isset($_GET["civ"]) || !isset($_GET["nom"]) || !isset($_GET["prenom"])) {
        echo "
        <form action='/form/exercice7.php' method='GET'>
            <div>
                <label for='civ'>Civilité : </label>
                <select id='civ' name='civ' require>
                    <option value='Mr'>Mr</option>
                    <option value='Mme'>Mme</option>
                </select>
            </div>
            <div>
                <label for='nom'>Nom : </label>
                <input type='text' id='nom' name='nom' require>
            </div>
            <div>
                <label for='prenom'>Prénom :</label>
                <input type='text' id='prenom' name='prenom' require>
            </div>
            <div>
                <label for='file'>Joindre : </label>
                <input type='file' id='file' name='file'>
            </div>
            <div>
                <button type='submit'>Envoyer</button>
            </div>
            
        </form>";
    }

    ?>