<!-- Créer un formulaire demandant le nom et le prénom. 
Ce formulaire doit rediriger vers la page user.php avec la méthode POST. -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <form action="/form/user.php" method="POST">
        <div>
            <label for="nom">Nom : </label>
            <input type="text" id="nom" name="nom" require>
        </div>
        <div>
            <label for="prenom">Prénom :</label>
            <input type="text" id="prenom" name="prenom" require>
        </div>
        <div>
            <button type="submit">Envoyer</button>
        </div>
        
    </form>

</body>
</html>