<!-- Faire une page HTML permettant de donner à l'utilisateur :

    son User Agent
    son adresse ip
    le nom du serveur -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <?php

        var_dump($_SERVER);
        echo "<p>User Agent : " . $_SERVER["HTTP_USER_AGENT"] . "</p>";
        echo "<p>IP : " . $_SERVER["REMOTE_ADDR"] . "</p>";
        echo "<p>Server name : " . $_SERVER["SERVER_NAME"] . "</p>";

        var_dump($GLOBALS);
    ?>

</body>
</html>