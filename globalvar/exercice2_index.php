<!-- Sur la page index, faire un liens vers une autre page. 
Passer d'une page à l'autre le contenu des variables nom, prenom et age grâce aux sessions. 
Ces variables auront été définies directement dans le code.
Il faudra afficher le contenu de ces variables sur la deuxième page. -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php

    $profil = array(
        nom => "Chevallier",
        prenom => "Lucas",
        age => 24
    );

    session_start();
    $_SESSION["session"]=$profil;
    
    ?>
    <a href="/globalvar/exercice2.php">Cliquez-ici</a>
</body>
</html>