<!-- Faire un formulaire qui permet de récupérer le login et le mot de passe de l'utilisateur. 
A la validation du formulaire, stocker les informations dans un cookie. -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <form method="POST" action="">
        <div>
            <label for="login">Login : </label>
            <input type="text" id="login" name="login" require>
        </div>
        <div>
            <label for="password">Mot de passe : </label>
            <input type="password" id="password" name="password" require>
        </div>
        <div>
            <button type="submit">Connexion</button>
        </div>
    </form>


    <?php

    setcookie("login",$_POST["login"]);
    setcookie("password",$_POST["password"]);

    ?>

</body>
</html>