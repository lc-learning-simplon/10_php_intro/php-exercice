<!-- Créer une variable et l'initialiser à 0.
Tant que cette variable n'atteint pas 10, il faut :

    - l'afficher
    - l'incrementer -->


<?php

$var = 0;

while ($var <= 10) {
    echo "<p>$var</p>";
    $var++;
}

?>
